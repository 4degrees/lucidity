# :coding: utf-8
# :copyright: Copyright (c) 2013 Martin Pengelly-Phillips
# :license: See LICENSE.txt.

import os
import re

from setuptools import find_packages, setup
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    '''Pytest command.'''

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        '''Import pytest and run.'''
        import pytest
        errno = pytest.main(self.test_args)
        raise SystemExit(errno)

here = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(here, 'source', 'lucidity', '_version.py')) as _version_file:
    VERSION = re.match(r'.*__version__ = \'(.*?)\'', _version_file.read(), re.DOTALL).group(1)

# Get the long description from the README file
with open(os.path.join(here, "README.rst")) as f:
    LONG_DESCRIPTION = f.read()

# Fetch run-time requirements
with open(os.path.join(here, "requirements.txt")) as f:
    REQUIRES = [line for line in f.read().splitlines() if not line.startswith("#")]

with open(os.path.join(here, "requirements", "dev.txt")) as f:
    DEV_REQUIRES = [line for line in f.read().splitlines() if not line.startswith("#")]

# Readthedocs requires Sphinx extensions to be specified as part of
# install_requires in order to build properly.
on_rtd = os.environ.get('READTHEDOCS', None) == 'True'
if on_rtd:
    REQUIRES.extend(DEV_REQUIRES)

setup(
    name='Lucidity',
    version=VERSION,
    description='Filesystem templating and management.',
    long_description=LONG_DESCRIPTION,
    keywords='filesystem, management, templating',
    url='https://gitlab.com/4degrees/lucidity',
    author='Martin Pengelly-Phillips',
    author_email='martin@4degrees.ltd.uk',
    license='Apache License (2.0)',
    packages=find_packages(where="source"),
    package_dir={
        '': 'source'
    },
    setup_requires=DEV_REQUIRES,
    install_requires=REQUIRES,
    tests_require=[
        'pytest >= 2.3.5'
    ],
    cmdclass={
        'test': PyTest
    },
    zip_safe=False
)
